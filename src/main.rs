use log::{debug, info, warn};
use std::{env, error::Error};
use tokio::stream::StreamExt;
use twilight::model::gateway::GatewayIntents;
use twilight::{
    cache::InMemoryCache,
    gateway::cluster::{config::ShardScheme, Cluster, Config},
    gateway::shard::Event,
    http::Client as HttpClient,
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("DISCORD_TOKEN")
        .expect("TOKEN NOT FOUND, REPORTING TO THE AUTHORITIES THAT YOU ARE A TERRIBLE PERSON");

    pretty_env_logger::init_timed();

    // This is also the default.
    let scheme = ShardScheme::Auto;

    let config = Config::builder(&token)
        .shard_scheme(scheme)
        .intents(Some(
            //            GatewayIntents::GUILD_MESSAGES | GatewayIntents::DIRECT_MESSAGES,
            GatewayIntents::all(),
        ))
        .build();

    let cluster = Cluster::new(config);

    cluster.up().await?;

    let http = HttpClient::new(&token);
    let cache = InMemoryCache::new();

    let mut events = cluster.events().await;

    let ctx = Context::new(&http, &cache);

    while let Some((id, event)) = events.next().await {
        println!("Shard: {}, Event: {:?}", id, event.event_type());

            ctx.cache.update(&event).await.expect("Cache failed, OhNoe");
            debug!("New Event: {:?}", event.event_type());
            // println!("{:?}", event);

            if let Err(val) = handle_event(event, &ctx).await {
                warn!("Error when handling event: {:#?}", val);
            }
    }

    Ok(())
}

async fn handle_event(event: Event, ctx: &Context<'_>) -> Result<(), Box<dyn Error>> {
    match event {
        Event::MessageCreate(msg) if msg.content == "e" => {
            ctx.http
                .create_reaction(msg.channel_id, msg.id, "wowcool:512671346270928908")
                .await?;
        }
        Event::ReactionAdd(f) if f.user_id == twilight::model::id::UserId(77812253511913472) => {
            ctx.http
                .delete_all_reaction(f.channel_id, f.message_id, "wowcool:512671346270928908")
                .await?
        }
        Event::MessageCreate(msg) if msg.content == "!ping" => {
            ctx.http
                .create_message(msg.channel_id)
                .content("Pong!")
                .await?;
            // hct.ttp.delete_all_reactions(
            //     twilight::model::id::ChannelId(355889026726887426),
            //     twilight::model::id::MessageId(655575553604321280),
            // );
            // ctx.http.create_message(msg.channel_id)
            //     .content(format!(
            //         "Current test response is: ``{:#?}``",
            //         cache
            //             .presence(
            //                 Some(twilight::model::id::GuildId(197169999494774784)),
            //                 twilight::model::id::UserId(197170995633717259)
            //             )
            //             .await?
            //             .unwrap()
            //             .game
            //             .as_ref()
            //             .unwrap()
            //             .name,
            //     ))
            //     .await?;
            // if let Some(guild) = cache
            //     .guild(twilight::model::id::GuildId(197169999494774784))
            //     .await?
            // {
            //     ctx.http.create_message(msg.channel_id)
            //         .content(format!("Current User is: {}", guild.id,))
            //         .await?;
            // } else {
            //     ctx.http.create_message(msg.channel_id)
            //         .content("no guild for u");
            // };
        }
        Event::ShardConnected(connected) => {
            // cache.update(&connected).await?;
            println!("Connected on shard {}", connected.shard_id);
        }
        _ => {}
    }

    Ok(())
}

struct Context<'a> {
    http: &'a HttpClient,
    cache: &'a InMemoryCache,
}

impl<'a> Context<'a> {
    fn new(http: &'a HttpClient, cache: &'a InMemoryCache) -> Self {
        Context {
            http,
            cache,
        }
    }
}
